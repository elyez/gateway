const net = require('net');
const assert = require('assert');

const Elyez = require(__root + 'lib/Elyez');
const PORT = process.env.PORT || 8888;

describe('Service', () => {
  this.app = Elyez();
  
  this.app
  .use((client, data, next) => {
    var str = data.raw.toString() + '-' + data.raw.toString();
    data.raw = str;
    next();
  })
  .use((client, data, next) => {
    data.id = 'foo';
    next();
  })
  .service(function(data, done) {
    assert.equal(`foo`, data.id);
    assert.equal(`undefined`, typeof data.raw);
    done();
  });
  
  before(done => {
    this.app.listen(PORT, done);
  });
  
  after(done => {
    this.app.close(done);
  });
  
  describe('Output to console', () => {
    var client = new net.Socket();
    
    it(`should be able to establish connection from client`, done => {
      client.connect(PORT, '127.0.0.1', done);
      client.on('error', done);
    });
    
    it(`should be able to access data id and but data raw is undefined`, done => {
      const str = 'Foo';
      client.write(str);
      client.destroy();
      done();
    });
    
    it(`should close client connection`, done => {
      client.destroy();
      done();
    });
  });
});