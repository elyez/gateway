const net = require('net');
const assert = require('assert');

const Elyez = require(__root + 'lib/Elyez');
const Parser = require(__root + 'src/parser/general');
const PORT = process.env.PORT || 8888;

describe('Data Parser', () => {
  this.app = Elyez();

  this.app.set('parser', Parser);

  before(done => {
    this.app.listen(PORT, done);
  });

  after(done => {
    this.app.close(done);
  });

  describe('General', () => {

  //describe('Sending Data', () => {
    var client = new net.Socket();

    it(`should be able to establish connection to server`, done => {
      client.connect(PORT, '127.0.0.1', done);
      client.on('error', done);
    });

    it(`should process single data`, done => {
      const str = new Buffer('$$,123456789086425,170131163459,101.12345,-6.09876,0,12,45,216,11321.56,24C9');
      client.write(str);

      var app = this.app;
      //fake network delay and app data changes
      setTimeout(function(){
          assert.equal(app.data.raw, str.toString());
          done();
      }, 250);
    });

    it(`should process bulk data`, done => {
      const str = new Buffer('$$,123456789086425,170131163459,101.12345,-6.09876,0,12,45,216,11321.56,24C9;$$,123456789086425,170131163511,101.12345,-6.09876,0,12,33,221,11324.01,24C9');
      client.write(str);

      var app = this.app;
      //fake network delay and app data changes
      setTimeout(function(){
          assert.equal(app.data.raw, str.toString());
          done();
      }, 250);
    });

    it(`should close client connection`, done => {
      client.destroy();
      done();
    });
  });
});