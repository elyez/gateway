# Elyez - GPS Gateway TCP/UDP Server

[![build status](https://gitlab.com/elyez/gateway/badges/master/build.svg)](https://gitlab.com/elyez/gateway/commits/master)
[![codecov](https://codecov.io/gl/elyez/gateway/branch/master/graph/badge.svg)](https://codecov.io/gl/elyez/gateway)
[![npm](https://badge.fury.io/js/%40elyez%2Fgateway.svg)](https://badge.fury.io/js/%40elyez%2Fgateway)


## Simple usage

```
const Elyez = require('@elyez/gateway');

const app = Elyez();
app.listen(PORT, function() {
  console.log('Server is running in %s mode port %s', app.options.mode || 'TCP', app.port );
});
```

### Options

```
const options = {
  mode: 'UDP'       //default: TCP
  encoding: 'hex'   //default: UTF-8
}
const app = Elyez(options);
```

#### .listen(port, [callback])

Start server listening on given port number

```
app.listen(process.env.PORT || 3000);

app.listen(process.env.PORT || 3000, function() {
  console.log('Server is running in %s mode port %s', app.options.mode || 'TCP', app.port );
});
```

#### .set(key, value)

Set new custom value

```
app
.set('my', value)
.set('anything', 'valuable')
.listen(process.env.PORT || 3000);
```

#### Parser

Parser function to formatted GPS Data

```
const Concox = require('@elyez/concox');

app
.set('parser', Concox.GT06N)
.listen(process.env.PORT || 3000);
```

## Middleware

Middleware runs in series. This is the main difference with `Service` described lower.
Blocking functions and run in sequential mode. The order matters and data will be changed depends on the process to next middleware functions.

#### .use(function(socket, data, next))

```
const myMiddleware = (socket, data, next) => {
  socket.name = 'foo';
  const rawdata = data.raw;
  //process data further
  // ...
  
  //continue to next middleware (important!)
  next();
};

app
.set('parser', Concox.GT06N)
.use(myMiddleware)
.listen(process.env.PORT || 3000);
```

Provide options to middleware, suits better for database adapters;

`src/middleware/database`

```
const MongoClient = require('mongodb').MongoClient;

module.exports = (app, options) => {
  if (!app.db) {
    const url = `mongodb://${username}:${password}@localhost:27017/${database}`;
    app.db = yield MongoClient.connect(url, {poolSize: options.poolSize || 10});
  }
  
  const collection = db.collection('documents');
  
  return function(socket, data, next){
    collection.updateOne( {id: data.id}, {$set: data}, {upsert: true, safe: false}, function(err,data){
        if (err) return next(err);
        
        console.log(data);
        next();
    })
  }
}
```

`app.js`

```
const Elyez = require('@elyez/gateway');
const Concox = require('@elyez/concox');
const database = require('./src/middleware/database');

const app = Elyez();
app
.set('encoding', 'hex')
.set('parser', Concox.GT06N)
.use(database(app, {
    username: 'user',
    password: 'pass',
    database: 'elyez'
  })
)
.listen(process.env.PORT || 3000);
```

## Service

Services run in parallel. Data passed to services is considered to be fixed and final.

#### .service(function(data, [done]))

Register service to the app

`src/service/console`

```
module.exports = (data, done) => {
    console.log(data);
    
    //optional call in the end each service
    done();
}
```

`src/service/webhook`

```
const request = require('request');

module.exports = (options) => {
  return function(data, next){
    request.post({url:options.url, formData: data});
  }
}

```

`app.js`

```
const Elyez    = require('@elyez/gateway');
const Concox   = require('@elyez/concox');
const database = require('./src/middleware/database');
const console  = require('./src/service/console');
const webhook  = require('./src/service/webhook');

const app = Elyez();
app
.set('encoding', 'hex')
.set('parser', Concox.GT06N)
.use(database(app, {
    username: 'user',
    password: 'pass',
    database: 'elyez'
  })
)
.service(console)
.service( webhook({url: 'https://my.anotherapp.com/webhook/elyez'}) )
.listen(process.env.PORT || 3000);
```


## Events

* **connect**
* **disconnect** (_alias_ **end**)
* **error**

`app.js`

```
const Elyez    = require('@elyez/gateway');
const Concox   = require('@elyez/concox');
const console  = require('./src/service/console');

const app = Elyez();

app.on('connect', client => {
  console.log(client, 'CONNECTED');
});
app.on('disconnect', client => {
  console.log(client, 'DISCONNECTED');
});
app.on('error', err => {
  console.error('Error:', err);
});
```


## TODO

* Send Commands


## License

The MIT License (MIT)