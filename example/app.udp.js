const Elyez = require('../lib/Elyez');
const Parser = require('../src/parser/general');
const echo = require('../src/middleware/echo');
const PORT = process.env.PORT || 8888;

// UDP Mode
const options = {
  mode: 'UDP'
};

const app = Elyez(options);

app.on('connect', client => {
  console.log(client, 'CONNECTED');
});
app.on('disconnect', client => {
  console.log(client, 'DISCONNECTED');
});
app.on('error', err => {
  console.error('Error:', err.message);
});

app
.set('parser', Parser)
.use(echo())
.service(function(data, done) {
  console.log(data);
  done();
})
.listen(PORT, function() {
  console.log('✮ %s %s', `[${app.options.mode || 'TCP'}:${app.port}]`, 'General GPS' );
});