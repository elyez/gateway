const net = require('net');
const assert = require('assert');

const Elyez = require(__root + 'lib/Elyez');
const PORT = process.env.PORT || 8888;
const echo = require(__root + 'src/middleware/echo');

describe('Middleware', () => {
  this.app = Elyez();
  this.app.use(echo());

  before(done => {
    this.app.listen(PORT, done);
  });

  after(done => {
    this.app.close(done);
  });

  describe('Echo', () => {
    var client = new net.Socket();

    it(`should be able to establish connection from client`, done => {
      client.connect(PORT, '0.0.0.0', done);
      client.on('error', done);
    });

    it(`should return echo from server`, done => {
      const str = 'WORD';
      client.connect(PORT, '0.0.0.0', done);
      client.on('error', done);
      client.on('data', data => {
          assert.equal(str, data.toString());
          done();
      });

      client.write(str);
    });

    it(`should accessible raw data from app`, done => {
      const str = 'WORD';
      assert.equal(str, this.app.data.raw.toString());
      done();
    });

    it(`should close client connection`, done => {
      client.destroy();
      done();
    });
  });
});