const net = require('net');
const path = require('path');
const assert = require('assert');

global.__root = path.join(__dirname, '../');

const Elyez = require(__root + 'lib/Elyez');
const PORT = process.env.PORT || 8888;

describe('Application', () => {
  this.app = Elyez();
  
  before(done => {
    this.app.listen(PORT, done);
  });
  
  after(done => {
    this.app.close(done);
  });
  
  describe('Server', () => {
    it(`should return port ${PORT} when server is running`, () => {
      assert.equal(PORT, this.app.port);
    });
    
    it(`should return error when new server run on the same port`, () => {
      const server2 = this.app.listen(PORT, () => done());
      assert.throws(function(){throw new Error('BOOM')}, Error);
    });
  });
  
  describe('Client', () => {
    var client = new net.Socket();
    it(`should able to establish connection from client`, done => {
        client.connect(PORT, '127.0.0.1', () => {
          client.destroy();
          done();
        });
        client.on('error', done);
    });
    
    it(`should close client connection`, done => {
      client.destroy();
      done();
    });
  });
});